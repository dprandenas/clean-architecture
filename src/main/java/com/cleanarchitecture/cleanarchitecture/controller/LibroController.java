package com.cleanarchitecture.cleanarchitecture.controller;

import com.cleanarchitecture.cleanarchitecture.controller.model.Libro;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/libros")
public class LibroController {

    private final List<Libro> libros = new ArrayList<>();

    @GetMapping
    public List<Libro> obtenerLibros() {
        return libros;
    }

    @PostMapping
    public void agregarLibro(@RequestBody Libro libro) {
        libros.add(libro);
    }

    @GetMapping("/{id}")
    public Libro obtenerLibroPorId(@PathVariable Long id) {
        return libros.stream()
                .filter(libro -> libro.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @DeleteMapping("/{id}")
    public void borrarLibro(@PathVariable Long id) {
        libros.removeIf(libro -> libro.getId().equals(id));
    }
}
