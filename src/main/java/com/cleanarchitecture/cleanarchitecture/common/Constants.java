package com.cleanarchitecture.cleanarchitecture.common;

public class Constants {

    public static final String TITLE = "Welcome Clean Architecture";
    public static final String DESCRIPTION_FIRST = "La Arquitectura limpia es buena para un código de calidad";

    public static String EMPTY_STRING = "";
}
