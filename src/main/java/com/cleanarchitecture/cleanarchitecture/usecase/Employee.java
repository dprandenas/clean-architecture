package com.cleanarchitecture.cleanarchitecture.usecase;

import com.cleanarchitecture.cleanarchitecture.common.Constants;

import java.util.Date;

public class Employee {

    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getStructureArchitectureMoreEmployeeName(){
        return Constants.TITLE + Constants.DESCRIPTION_FIRST  + name;
    }

    public String getNameAndDate2(){
        return new Date() + name;
    }
}
