package com.cleanarchitecture.cleanarchitecture.usecase;

import com.cleanarchitecture.cleanarchitecture.common.Constants;
import com.cleanarchitecture.cleanarchitecture.common.FormatsDateUtils;

//@Entity
public class Persona {

    private String name;

    public Persona(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getStructureArchitectureMorePersonName(){
        return Constants.TITLE + Constants.DESCRIPTION_FIRST  + name;
    }

    public String getNameAndDate(){
        return FormatsDateUtils.getDateNow() + name;
    }
}
